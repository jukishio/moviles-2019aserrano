
class singleton {
    ...
    companion object {
        const val abc = "ABC"
    }
}

object Singletons {

    val anotherClass = singleton()
}

fun main(args: Array<String>) {

    val b = Singletons.singleton
}