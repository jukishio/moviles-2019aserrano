package patterns

interface CarService {
    fun doService()
}

interface CarServiceDecorator : CarService

class BasicCarService : CarService {
    override fun doService() = println("Doing basic checkup ... DONE")
}

class CarWash(private val carService: CarService) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Washing car ... DONE")
    }
}

class InsideCarCleanup(private val carService: CarService) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Cleaning car inside ... DONE")
    }
}

fun main(args: Array<String>) {
    val carService = InsideCarCleanup(CarWash(BasicCarService()))
    carService.doService()
}